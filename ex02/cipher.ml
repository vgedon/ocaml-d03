
let rot42 str = 
	String.map (fun x -> let a = (int_of_char (x)) + 42 - 32 in
						if a > (126 - 32) then char_of_int(a mod (127 - 32) + 32)
						else char_of_int (a + 32))
				str

let ceasar str n =
	String.map (fun x -> let a = (int_of_char (x)) + n - 32 in
						if a > (126 - 32) then char_of_int(a mod (127 - 32) + 32)
						else char_of_int (a + 32))
				str

let xor str key =
	String.map (fun x -> char_of_int( int_of_char (x) lxor key ) ) str

let rec ft_crypt str f = match f with
| [] -> str
| head::tail -> ft_crypt (head str) tail


(* ********************************************************************** *)

(* rot42 *)
let () = print_string "rot42 \"\" => "; print_endline (rot42 "")
let () = print_string "rot42 \"bonjour tout le monde!\" => "; print_endline (rot42 "bonjour tout le monde!")
let () = print_string "rot42 \"abcdefghijklmnopqrstuvwxyz\" => "; print_endline (rot42 "abcdefghijklmnopqrstuvwxyz")
let () = print_string "rot42 \"Immondis, Lovenpis, Serge\" => "; print_endline (rot42 "Immondis, Lovenpis, Serge")
let () = print_newline ()

(* ceasar *)
let () = print_string "ceasar \"\" 10 => "; print_endline (ceasar "" 10)
let () = print_string "ceasar \"Et vous signez cisar\" 0 => "; print_endline (ceasar "Et vous signez cisar" 0)
let () = print_string "ceasar \"Et vous signez cisar\" 10 => "; print_endline (ceasar "Et vous signez cisar" 10)
let () = print_string "ceasar \"Manu BestofPlus\" 20 => "; print_endline (ceasar "Manu BestofPlus" 20)
let () = print_string "ceasar \"Immondis, Lovenpis, Serge\" 30 => "; print_endline (ceasar "Immondis, Lovenpis, Serge" 30)
let () = print_string "ceasar \"abcdefghijklmnopqrstuvwxyz\" 42 => "; print_endline (ceasar "abcdefghijklmnopqrstuvwxyz" 42)
let () = print_string "ceasar \"Hue Cannabis\" 255 => "; print_endline (ceasar "Hue Cannabis" 255)
let () = print_newline ()

(* xor *)
let () = print_string "xor \"Pancakes\" 0 => "; print_endline (xor "Pancakes" 0)
let () = print_string "xor \"\" 2 => "; print_endline (xor "" 2)
let () = print_string "xor \"Pancakes\" 2 => "; print_endline (xor "Pancakes" 2)
let () = print_string "xor \"Pancakes\" 5 => "; print_endline (xor "Pancakes" 5)
let () = print_newline ()

(* ft_crypt *)

let () = print_string "ft_crypt \"Immondis, Lovenpis, Serge\" [.] => "; print_endline (ft_crypt "Immondis, Lovenpis, Serge" [rot42])
let () = print_string "ft_crypt \"Immondis, Lovenpis, Serge\" [..] => "; print_endline (ft_crypt "Immondis, Lovenpis, Serge" [rot42; (fun str -> ceasar str 30)])
let () = print_string "ft_crypt \"Immondis, Lovenpis, Serge\" [...] => "; print_endline (ft_crypt "Immondis, Lovenpis, Serge" [rot42; (fun str -> ceasar str 30); (fun str -> xor str 2)])


(* Uncipher.unrot42 *)
let () = print_string "Uncipher.unrot42 \"\" => "; print_endline (Uncipher.unrot42 "")
let () = print_string "Uncipher.unrot42 \"bonjour tout le monde!\" => "; print_endline (Uncipher.unrot42 "bonjour tout le monde!")
let () = print_string "Uncipher.unrot42 \"abcdefghijklmnopqrstuvwxyz\" => "; print_endline (Uncipher.unrot42 "abcdefghijklmnopqrstuvwxyz")
let () = print_string "Uncipher.unrot42 \"Immondis, Lovenpis, Serge\" => "; print_endline (Uncipher.unrot42 "Immondis, Lovenpis, Serge")
let () = print_newline ()

(* Uncipher.unceasar *)
let () = print_string "Uncipher.unceasar \"\" 10 => "; print_endline (Uncipher.unceasar "" 10)
let () = print_string "Uncipher.unceasar \"Et vous signez cisar\" 0 => "; print_endline (Uncipher.unceasar "Et vous signez cisar" 0)
let () = print_string "Uncipher.unceasar \"Et vous signez cisar\" 10 => "; print_endline (Uncipher.unceasar "Et vous signez cisar" 10)
let () = print_string "Uncipher.unceasar \"Manu BestofPlus\" 20 => "; print_endline (Uncipher.unceasar "Manu BestofPlus" 20)
let () = print_string "Uncipher.unceasar \"Immondis, Lovenpis, Serge\" 30 => "; print_endline (Uncipher.unceasar "Immondis, Lovenpis, Serge" 30)
let () = print_string "Uncipher.unceasar \"abcdefghijklmnopqrstuvwxyz\" 42 => "; print_endline (Uncipher.unceasar "abcdefghijklmnopqrstuvwxyz" 42)
let () = print_string "Uncipher.unceasar \"Hue Cannabis\" 255 => "; print_endline (Uncipher.unceasar "Hue Cannabis" 255)
let () = print_newline ()

(* Uncipher.ft_uncrypt *)

let () = print_string "Uncipher.ft_uncrypt \"s88:9/4>VJv:A09;4>VJ}0=20\" [.] => "; print_endline (Uncipher.ft_uncrypt "s88:9/4>VJv:A09;4>VJ}0=20" [Uncipher.unrot42])
let () = print_string "Uncipher.ft_uncrypt \"2VVXWMR\\th5X_NWYR\\th<N[PN\" [..] => "; print_endline (Uncipher.ft_uncrypt "2VVXWMR\\th5X_NWYR\\th<N[PN" [Uncipher.unrot42; (fun str -> Uncipher.unceasar str 30)])
let () = print_string "Uncipher.ft_uncrypt \"0TTZUOP^vj7Z]LU[P^vj>LYRL\" [...] => "; print_endline (Uncipher.ft_uncrypt "0TTZUOP^vj7Z]LU[P^vj>LYRL" [Uncipher.unrot42; (fun str -> Uncipher.unceasar str 30); (fun str -> xor str 2)])