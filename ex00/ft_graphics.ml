type 'a tree = Nil | Node of 'a * 'a tree * 'a tree 

let create_win width height =
	Graphics.open_graph (" " ^ ((string_of_int width) ^  "x" ^ (string_of_int height)))

(*Graphics.open_graph " 100x100"*)

let draw_square x y size =
	Graphics.moveto x y;
	let x1 = (x - (size / 2))
	in
	let y1 = (y - (size / 2))
	in  
		begin
			Graphics.moveto x1 y1;
			Graphics.lineto (x1 + size) y1 ;
			Graphics.lineto (x1 + size) (y1 + size);
			Graphics.lineto x1 (y1 + size);
			Graphics.lineto x1 y1;
		end

let draw_tree_node n =
	match n with
	| Nil -> ()
	| Node (value, _, _) ->
		begin
			draw_square 25 75 50;
			Graphics.moveto 25 75;
			Graphics.draw_string value;
			draw_square 125 50 50;
			Graphics.moveto 125 50;
			Graphics.draw_string "Nil";
			draw_square 125 125 50;
			Graphics.moveto 125 125;
			Graphics.draw_string "Nil";
			Graphics.moveto 50 75;
			Graphics.lineto 100 50;
			Graphics.moveto 50 75;
			Graphics.lineto 100 125
		end


let main () =
	let n =  Node("value", Nil, Nil)in 
	create_win ((50+1) * 3) ((50 + 1) * 3);
	draw_tree_node n;
	(*	draw_square 25 50 50; (*x =  1/4 width, y = 1/2 height*)
		draw_square 75 25 50; (*x =  3/4 width, y = 1/4 height*)
		draw_square 75 75 50; (*x =  3/4 width, y = 3/4 height*)
*)
	Graphics.read_key ()

let _ = main ()