
let unrot42 str = 
	String.map (fun x -> let a = (int_of_char (x)) - 42 - 32 in
						if a < 0 then char_of_int(a mod (127 - 32) + 127)
						else char_of_int (a + 32))
				str

let unceasar str n =
	String.map (fun x -> let a = (int_of_char (x)) - n - 32 in
						if a < 0 then char_of_int(a mod (127 - 32) + 127)
						else char_of_int (a + 32))
				str

let rec ft_uncrypt str f = match f with
| [] -> str
| head::tail -> head (ft_uncrypt str tail)


(* ********************************************************************** *)
